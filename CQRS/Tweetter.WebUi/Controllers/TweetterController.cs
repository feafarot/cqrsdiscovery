﻿namespace Tweetter.WebUi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;

    using Tweetter.Domain.DataReadModel;
    using Tweetter.Domain.EntityCommands;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public class TweetterController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewData.Model = ServiceLocator.ReadModel.GetAllTweets();
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        } 

        [HttpPost]
        public ActionResult Create(TweetDto tweet)
        {
            try
            {
                ServiceLocator.Bus.Send(new CreateTweetCommand(tweet.Body, (Guid)this.Session["UserId"]));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var tweetDto = ServiceLocator.ReadModel.GetTweetDtolById(id);
            ViewData.Model = tweetDto;
            return View();
        }

        [HttpPost]
        public ActionResult Edit(TweetDto tweetDto)
        {
            try
            {
                var tweet = ServiceLocator.ReadModel.GetTweetDtolById(tweetDto.Id);
                ServiceLocator.Bus.Send(new ChangeTweetBodyCommand(tweetDto.Id, tweetDto.Body, tweet.Version));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
