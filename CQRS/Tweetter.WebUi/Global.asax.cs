﻿namespace Tweetter.WebUi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Cqrs.Core;
    using Cqrs.Core.Data;
    using Cqrs.Core.Events;
    using Domain.DataReadModel;
    using Domain.Entities;
    using Domain.EntityCommands;
    using Domain.EntityCommands.Handlers;
    using Domain.EntityEvents;

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Tweetter", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            var dataBase = new FakeTweetDatabase();

            var readModel = new ReadModelFacade(dataBase);

            ServiceLocator.ReadModel = readModel;
            ServiceLocator.Bus = InitializeBus(dataBase);
        }

        protected void Session_Start()
        {
            this.Session.Add("UserId", Guid.NewGuid());
        }

        private static BaseBus InitializeBus(IDatabase<TweetDto> dataBase)
        {
            var bus = new BaseBus();
            var eventStorage = new EventStorage(bus);
            var repository = new Repository<Tweet>(eventStorage);
            var tweetCommandHandler = new TweetCommandHandlers(repository);
            bus.RegisterHandler<ChangeTweetBodyCommand>(tweetCommandHandler.Handle);
            bus.RegisterHandler<CreateTweetCommand>(tweetCommandHandler.Handle);

            var tweetDto = new TweetDetailView(dataBase);
            bus.RegisterHandler<TweetBodyChangedEvent>(tweetDto.Handle);
            bus.RegisterHandler<TweetCreatedEvent>(tweetDto.Handle);

            return bus;
        }
    }
}