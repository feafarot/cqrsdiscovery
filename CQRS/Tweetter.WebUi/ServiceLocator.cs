﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceLocator.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   Defines the ServiceLocator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.WebUi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Tweetter.Cqrs.Core;
    using Tweetter.Domain.DataReadModel;

    /// <summary>
    /// Fake service locator.
    /// </summary>
    public class ServiceLocator
    {
        /// <summary>
        /// Gets the event bus.
        /// </summary>
        public static BaseBus Bus { get; set; }

        /// <summary>
        /// Gets or sets the read model.
        /// </summary>
        public static IReadModelFacade ReadModel { get; set; }
    }
}