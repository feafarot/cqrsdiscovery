﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CoreTests.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Domain.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using NUnit.Framework;

    /// <summary>
    /// Unit test for Core.
    /// </summary>
    [TestFixture]
    public class CoreTests 
    {
        [Test]
        public void LinqForeach()
        {
            var list = new List<Foo> { new Foo { Name = "first" }, new Foo { Name = "second" } };
            list.ForEach(x => x.Name = x.Name + "(update)");
            foreach (var foo in list)
            {
                Assert.That(foo.Name, Is.EqualTo("first(updatre)"), "Invalid collection item.");
            }
        }
    }
}
