﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalSettings.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Global settings class.
    /// </summary>
    public class GlobalSettings
    {
        /// <summary>
        /// Actual instance of settings.
        /// </summary>
        private static GlobalSettings instance;

        private GlobalSettings()
        {
            this.CountOfChangesBeforeSnapshot = 3;
        }

        /// <summary>
        /// Gets the current settings.
        /// </summary>
        public static GlobalSettings Current
        {
            get
            {
                return instance = instance ?? new GlobalSettings();
            }
        }

        /// <summary>
        /// Gets or sets the count of changes before snapshot.
        /// </summary>
        public int CountOfChangesBeforeSnapshot { get; set; }
    }
}
