﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AggregateRoot.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Base aggregate root object.
    /// </summary>
    [Serializable]
    public abstract class AggregateRoot
    {
        /// <summary>
        /// Collection of changes.
        /// </summary>
        [NonSerialized]
        private List<Event> changes;
        
        private bool canCycle = false;

        private int countOfChangesFromSnapshot;

        /// <summary>
        /// Initializes a new instance of the <see cref="AggregateRoot"/> class.
        /// </summary>
        protected AggregateRoot()
        {
            this.Init();
        }

        /// <summary>
        /// Gets the unique item id.
        /// </summary>
        public abstract Guid Id { get; }

        /// <summary>
        /// Gets the count of changes from last snapshot.
        /// </summary>
        public int CountOfChangesFromLastSnapshot
        {
            get
            {
                return this.countOfChangesFromSnapshot;
            }
        }

        /// <summary>
        /// Gets the uncommitted changes.
        /// </summary>
        /// <returns>Uncommitted changes.</returns>
        public IEnumerable<Event> GetUncommittedChanges()
        {
            return this.changes;
        }

        /// <summary>
        /// Restores from history.
        /// </summary>
        /// <param name="history">The history.</param>
        public void RestoreFromHistory(IEnumerable<Event> history)
        {
            history.ForEach(x => this.ApplyChange(x, false));
        }

        /// <summary>
        /// Inits this instance.
        /// </summary>
        public void Init()
        {
            this.changes = new List<Event>();
            this.countOfChangesFromSnapshot = 0;
        }

        /// <summary>
        /// Marks the all changes as committed.
        /// </summary>
        public void MarkChangesAsCommitted()
        {
            this.changes.Clear();
        }

        /// <summary>
        /// Applies the specified event changes.
        /// </summary>
        /// <param name="event">The event that contains changes.</param>
        public virtual void Apply(Event @event)
        {
            if (@event is ISnapshot)
            {
                @event.Version = this.changes.Max(x => x.Version) + 1;
                this.changes.Add(@event);
                this.countOfChangesFromSnapshot = 0;
#if DEBUG
                Debug.WriteLine(string.Format("Create snapshot! Version: {0}.", @event.Version));
#endif
                return;
            }

            if (!this.canCycle)
            {
                this.canCycle = true;
                this.GetType().GetMethod("Apply", new[] { @event.GetType() }).Invoke(this, new[] { @event });
            }

            this.canCycle = false;
        }

        /// <summary>
        /// Recreates this instance.
        /// </summary>
        public abstract void Recreate();

        /// <summary>
        /// Applies the change from event.
        /// </summary>
        /// <param name="event">The @event.</param>
        protected void ApplyChange(Event @event)
        {
            this.ApplyChange(@event, true);
        }

        /// <summary>
        /// Applies the change from event.
        /// </summary>
        /// <param name="event">The event with changes.</param>
        /// <param name="isNew">If item is new.</param>
        private void ApplyChange(Event @event, bool isNew)
        {
            this.countOfChangesFromSnapshot++;

            this.Apply(@event);
            if (isNew)
            {
                this.changes.Add(@event);
            }
        }
    }
}
