﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHandles.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.ReadDataModel
{
    /// <summary>
    /// Handles message.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    public interface IHandles<in TMessage>
    {
        /// <summary>
        /// Handles the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Handle(TMessage message);
    }
}
