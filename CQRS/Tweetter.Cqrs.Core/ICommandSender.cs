﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandSender.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Commands;

    /// <summary>
    /// Command sender.
    /// </summary>
    public interface ICommandSender
    {
        /// <summary>
        /// Sends the specified command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        /// <param name="command">The command.</param>
        void Send<TCommand>(TCommand command) where TCommand : Command;
    }
}
