﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseSnapshotEvent.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Events
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Soap;
    using System.Text;

    using Newtonsoft.Json;

    using Tweetter.Cqrs.Core.Utils;

    /// <summary>
    /// Base snapshot event class.
    /// </summary>
    /// <typeparam name="TAggregate">The type of the aggregate.</typeparam>
    public class BaseSnapshotEvent<TAggregate> : Event, ISnapshot<TAggregate> where TAggregate : AggregateRoot
    {
        private byte[] serializedAggregate = null;

        /// <summary>
        /// Gets or sets the serialized aggregate.
        /// </summary>
        public TAggregate SerializedAggregate
        {
            get
            {
                return this.Deserialize();
            }

            set
            {
                this.Serialize(value);
            }
        }

        private TAggregate Deserialize()
        {
            ////using (var stream = new MemoryStream())
            ////{
            ////    var writer = new StreamWriter(stream);
            ////    writer.Write(this.serializedAggregate);
            ////    var formatter = new SoapFormatter();
            ////    return formatter.Deserialize(stream) as TAggregate;
            ////}
            return BinarySerializer.BinaryDeSerialize<TAggregate>(this.serializedAggregate);
        }

        private void Serialize(TAggregate aggregate)
        {
            ////using (var stream = new MemoryStream())
            ////{
            ////    var formatter = new SoapFormatter();
            ////    formatter.Serialize(stream, aggregate);
            ////    var reader = new StreamReader(stream);
            ////    this.serializedAggregate = reader.ReadToEnd();
            ////}
            this.serializedAggregate = BinarySerializer.BinarySerialize(aggregate);
        }
    }
}
