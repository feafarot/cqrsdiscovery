﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventDescriptor.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Event descriptor.
    /// </summary>
    public struct EventDescriptor
    {
        /// <summary>
        /// Event Data.
        /// </summary>
        public readonly Event EventData;

        /// <summary>
        /// Descriptor id.
        /// </summary>
        public readonly Guid Id;

        /// <summary>
        /// Descriptor version.
        /// </summary>
        public readonly int Version;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventDescriptor"/> struct.
        /// </summary>
        /// <param name="id">Descriptor id.</param>
        /// <param name="eventData">The event data.</param>
        /// <param name="version">Descriptor version.</param>
        public EventDescriptor(Guid id, Event eventData, int version)
        {
            this.EventData = eventData;
            this.Version = version;
            this.Id = id;
        }
    }
}
