﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Event.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Tweetter.Cqrs.Core;

    /// <summary>
    /// Base event class
    /// </summary>
    public class Event : IMessage
    {
        /// <summary>
        /// Gets or sets event version.
        /// </summary>
        public int Version { get; set; }
    }
}
