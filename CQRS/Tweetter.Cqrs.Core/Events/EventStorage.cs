﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventStorage.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Base event storage.
    /// </summary>
    public class EventStorage : IEventStorage
    {
        private readonly Dictionary<Guid, List<EventDescriptor>> current;

        private readonly IEventPublisher publisher;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventStorage"/> class.
        /// </summary>
        /// <param name="publisher">The publisher.</param>
        public EventStorage(IEventPublisher publisher)
        {
            this.publisher = publisher;
            this.current = new Dictionary<Guid, List<EventDescriptor>>();
        }

        /// <summary>
        /// Gets the events for aggregate.
        /// </summary>
        /// <param name="aggregateId">The aggregate id.</param>
        /// <returns>
        /// Event collection.
        /// </returns>
        public virtual IEnumerable<Event> GetEventsForAggregate(Guid aggregateId)
        {
            List<EventDescriptor> eventDescriptors;
            if (!this.current.TryGetValue(aggregateId, out eventDescriptors))
            {
                throw new AggregateNotFoundException();
            }

            return eventDescriptors.Select(d => d.EventData);
        }

        /// <summary>
        /// Gets the last snapshot for aggregate.
        /// </summary>
        /// <param name="aggregateId">The aggregate id.</param>
        /// <returns>
        /// Returns aggregate snapshot or null if it doesn't exist.
        /// </returns>
        public virtual Event GetLastSnapshotForAggregate(Guid aggregateId)
        {
            List<EventDescriptor> eventDescriptors;
            if (!this.current.TryGetValue(aggregateId, out eventDescriptors))
            {
                throw new AggregateNotFoundException();
            }
            
            return eventDescriptors.LastOrDefault(x => (x.EventData as ISnapshot) != null).EventData;
        }

        /// <summary>
        /// Saves the events.
        /// </summary>
        /// <param name="aggregateId">The aggregate item id.</param>
        /// <param name="events">The events.</param>
        /// <param name="expectedVersion">The expected version.</param>
        public virtual void SaveEvents(Guid aggregateId, IEnumerable<Event> events, int expectedVersion)
        {
            List<EventDescriptor> eventDescriptors;
            if (!this.current.TryGetValue(aggregateId, out eventDescriptors))
            {
                eventDescriptors = new List<EventDescriptor>();
                this.current.Add(aggregateId, eventDescriptors);
            }
            else if (eventDescriptors[eventDescriptors.Count - 1].Version != expectedVersion && expectedVersion != -1)
            {
                throw new ConcurrencyException();
            }

            var version = expectedVersion;
            foreach (var @event in events)
            {
                version = @event is ISnapshot ? version : version + 1;
                @event.Version = version;
                eventDescriptors.Add(new EventDescriptor(aggregateId, @event, version));
                this.publisher.Publish(@event);
            }
        }
    }
}
