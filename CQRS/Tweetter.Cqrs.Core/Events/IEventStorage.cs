﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventStorage.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Provides operations with events.
    /// </summary>
    public interface IEventStorage
    {
        /// <summary>
        /// Gets the events for aggregate.
        /// </summary>
        /// <param name="aggregateId">The aggregate id.</param>
        /// <returns>Event collection.</returns>
        IEnumerable<Event> GetEventsForAggregate(Guid aggregateId);

        /// <summary>
        /// Gets the last snapshot for aggregate. 
        /// </summary>
        /// <param name="aggregateId">The aggregate id.</param>
        /// <returns>Returns aggregate snapshot or null if it doesn't exist.</returns>
        Event GetLastSnapshotForAggregate(Guid aggregateId);

        /// <summary>
        /// Saves the events.
        /// </summary>
        /// <param name="aggregateId">The aggregate item id.</param>
        /// <param name="events">The events.</param>
        /// <param name="expectedVersion">The expected version.</param>
        void SaveEvents(Guid aggregateId, IEnumerable<Event> events, int expectedVersion);
    }
}
