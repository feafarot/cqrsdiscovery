﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISnapshot.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Snapshot interface.
    /// </summary>
    /// <typeparam name="TAggreagate">The type of the aggreagate.</typeparam>
    public interface ISnapshot<TAggreagate> : ISnapshot where TAggreagate : AggregateRoot
    {
        /// <summary>
        /// Gets or sets the serialized aggregate.
        /// </summary>
        TAggreagate SerializedAggregate { get; set; }
    }

    /// <summary>
    /// Base snapshot interface.
    /// You shouldn't use this interface.
    /// </summary>
    public interface ISnapshot
    {
    }
}
