﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseBus.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using Tweetter.Cqrs.Core;
    using Tweetter.Cqrs.Core.Commands;
    using Tweetter.Cqrs.Core.Events;
    using Tweetter.Cqrs.Core.Utils;

    /// <summary>
    /// Event bus(Fake). Should be reworked.
    /// </summary>
    public class BaseBus : ICommandSender, IEventPublisher
    {
        private readonly Dictionary<Type, List<Action<IMessage>>> routes;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseBus"/> class.
        /// </summary>
        public BaseBus()
        {
            this.routes = new Dictionary<Type, List<Action<IMessage>>>();
        }

        /// <summary>
        /// Registers the message handler.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="handler">The message handler.</param>
        public virtual void RegisterHandler<TMessage>(Action<TMessage> handler) where TMessage : IMessage
        {
            List<Action<IMessage>> handlers;
            if (!this.routes.TryGetValue(typeof(TMessage), out handlers))
            {
                handlers = new List<Action<IMessage>>();
                this.routes.Add(typeof(TMessage), handlers);
            }

            handlers.Add(DelegateAdjuster.CastArgument<IMessage, TMessage>(x => handler(x)));
        }

        /// <summary>
        /// Sends the specified command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        /// <param name="command">The command.</param>
        public virtual void Send<TCommand>(TCommand command) where TCommand : Command
        {
            List<Action<IMessage>> handlers;
            if (this.routes.TryGetValue(typeof(TCommand), out handlers))
            {
                if (handlers.Count != 1)
                {
                    throw new InvalidOperationException("Cannot send to more than one handler.");
                }

                handlers[0](command);
            }
            else
            {
                throw new InvalidOperationException("There are no registered handlers.");
            }
        }

        /// <summary>
        /// Publishes the specified event.
        /// </summary>
        /// <typeparam name="TEvent">The type of the event.</typeparam>
        /// <param name="event">The event to publish.</param>
        public virtual void Publish<TEvent>(TEvent @event) where TEvent : Event
        {
            List<Action<IMessage>> handlers;
            if (!this.routes.TryGetValue(@event.GetType(), out handlers))
            {
                return;
            }

            foreach (var handler in handlers)
            {
                handler(@event);
            }
        }
    }
}
