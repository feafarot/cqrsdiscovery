﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionsHelper.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Checks data and throws exception if it's need.
    /// </summary>
    public static class ExceptionsHelper
    {
        /// <summary>
        /// Checks the string for empy.
        /// </summary>
        /// <param name="data">The data.</param>
        public static void CheckStringForEmpy(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new AggregateException("Parameter mustn't be null or empty.");
            }
        }

        /// <summary>
        /// Checks the string for empty.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="errorMessage">The error message.</param>
        public static void CheckStringForEmpy(string data, string errorMessage)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new AggregateException(errorMessage);
            }
        }
    }
}
