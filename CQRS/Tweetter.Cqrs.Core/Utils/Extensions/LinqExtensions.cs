﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqExtensions.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace System.Linq
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;

    /// <summary>
    /// Extension methods for IEnumerable{T}.
    /// </summary>
    public static class LinqExtensions
    {
        /// <summary>
        /// Invokes expression for each item in collection.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source collection.</param>
        /// <param name="expression">The expression that should be called.</param>
        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> expression)
        {
            foreach (var item in source)
            {
                expression.Invoke(item);
            }
        }
    }
}
