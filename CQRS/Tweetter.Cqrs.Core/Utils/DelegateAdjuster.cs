﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DelegateAdjuster.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;

    /// <summary>
    /// The delegate adjuster.
    /// </summary>
    public class DelegateAdjuster
    {
        /// <summary>
        /// Casts the argument of action.
        /// </summary>
        /// <typeparam name="BaseT">The type of the ase T.</typeparam>
        /// <typeparam name="DerivedT">The type of the erived T.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>Casted action.</returns>
        public static Action<BaseT> CastArgument<BaseT, DerivedT>(Expression<Action<DerivedT>> source)
            where DerivedT : BaseT
        {
            if (typeof(DerivedT) == typeof(BaseT))
            {
                return (Action<BaseT>)((Delegate)source.Compile());
            }

            ParameterExpression sourceParameter = Expression.Parameter(typeof(BaseT), "source");
            var result =
                Expression.Lambda<Action<BaseT>>(
                    Expression.Invoke(source, Expression.Convert(sourceParameter, typeof(DerivedT))), sourceParameter);
            return result.Compile();
        }
    }
}
