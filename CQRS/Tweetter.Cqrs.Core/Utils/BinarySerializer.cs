﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BinarySerializer.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Utils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class BinarySerializer
    {
        /// <summary>
        /// Binaries the serialize.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>Serialized object.</returns>
        public static byte[] BinarySerialize(Object obj)
        {
            byte[] serializedObject;
            MemoryStream ms = new MemoryStream();
            BinaryFormatter b = new BinaryFormatter();
            b.Serialize(ms, obj);
            ms.Seek(0, 0);
            serializedObject = ms.ToArray();
            ms.Close();
            return serializedObject;
        }

        /// <summary>
        /// Binaries the de serialize.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="serializedObject">The serialized object.</param>
        /// <returns>Deserialized object.</returns>
        public static T BinaryDeSerialize<T>(byte[] serializedObject)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(serializedObject, 0, serializedObject.Length);
            ms.Seek(0, 0);
            BinaryFormatter b = new BinaryFormatter();
            Object obj = b.Deserialize(ms);
            ms.Close();
            return (T)obj;
        }
    }
}
