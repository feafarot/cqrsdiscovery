﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandSender.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Command sender interface.
    /// </summary>
    public interface ICommandSender
    {
        /// <summary>
        /// Sends the specified command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        /// <param name="command">Command for send.</param>
        void Send<TCommand>(TCommand command) where TCommand : Command;
    }
}
