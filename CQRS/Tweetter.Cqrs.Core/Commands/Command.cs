﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Command.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Cqrs.Core;
    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Base command class.
    /// </summary>
    public class Command : IMessage
    {
    }
}
