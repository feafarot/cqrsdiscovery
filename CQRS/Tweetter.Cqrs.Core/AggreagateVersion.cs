﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AggreagateVersion.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   Stores version of any item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core
{
    using System;

    /// <summary>
    /// Stores version of any item.
    /// </summary>
    public struct AggreagateVersion
    {
        private DateTime timeStamp;

        private int version;

        /// <summary>
        /// Initializes a new instance of the <see cref="AggreagateVersion"/> struct.
        /// </summary>
        /// <param name="version">The version.</param>
        public AggreagateVersion(int version)
        {
            this.version = version;
            this.timeStamp = DateTime.UtcNow;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AggreagateVersion"/> struct.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <param name="timeStamp">The time stamp.</param>
        public AggreagateVersion(int version, DateTime timeStamp)
        {
            this.version = version;
            this.timeStamp = timeStamp;
        }

        /// <summary>
        /// Gets the new.
        /// </summary>
        public static AggreagateVersion New
        {
            get
            {
                return new AggreagateVersion(0);
            }
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public int Version
        {
            get
            {
                return this.version;
            }

            set
            {
                this.version = value;
            }
        }

        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        public DateTime TimeStamp
        {
            get
            {
                return this.timeStamp;
            }

            set
            {
                this.timeStamp = value;
            }
        }
    }
}