﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Repository interface.
    /// </summary>
    /// <typeparam name="TRoot">Aggregated root object.</typeparam>
    public interface IRepository<TRoot> where TRoot : AggregateRoot, new()
    {
        /// <summary>
        /// Gets the item by it's aggregateId.
        /// </summary>
        /// <param name="aggregateId">The aggregateId.</param>
        /// <returns>Item from data.</returns>
        TRoot GetById(Guid aggregateId);

        /// <summary>
        /// Saves the specified aggregate object.
        /// </summary>
        /// <param name="aggregate">The aggregate for save.</param>
        /// <param name="expectedVersion">The expected version.</param>
        void Save(TRoot aggregate, int expectedVersion);
    }
}
