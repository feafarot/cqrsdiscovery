﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Repository.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Cqrs.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Sample repository.
    /// </summary>
    /// <typeparam name="TRoot">The type of the root.</typeparam>
    public class Repository<TRoot> : IRepository<TRoot> where TRoot : AggregateRoot, new()
    {
        /// <summary>
        /// Event storage.
        /// </summary>
        private readonly IEventStorage storage;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{TRoot}"/> class. 
        /// </summary>
        /// <param name="storage">
        /// The storage of events.
        /// </param>
        public Repository(IEventStorage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        /// Gets the item by it's aggregateId.
        /// </summary>
        /// <param name="aggregateId">The aggregateId of item.</param>
        /// <returns>Aggregated item.</returns>
        public TRoot GetById(Guid aggregateId)
        {
            TRoot item = null;
            var restoredFromSnapshot = false;
            var snapshot = this.storage.GetLastSnapshotForAggregate(aggregateId) as ISnapshot<TRoot>;
            if (snapshot != null)
            {
                item = snapshot.SerializedAggregate;
                item.Init();
                restoredFromSnapshot = true;
            }

            if (item == null)
            {
                item = new TRoot();
            }

            IEnumerable<Event> events;
            if (restoredFromSnapshot)
            {
                events = this.storage.GetEventsForAggregate(aggregateId).Where(x => x.Version > ((Event)snapshot).Version);
            }
            else
            {
                events = this.storage.GetEventsForAggregate(aggregateId);   
            }
            
            item.RestoreFromHistory(events);
            return item;
        }

        /// <summary>
        /// Saves the specified aggregate item.
        /// </summary>
        /// <param name="aggregate">The aggregate item.</param>
        /// <param name="expectedVersion">The expected item version.</param>
        public void Save(TRoot aggregate, int expectedVersion)
        {
            int version = expectedVersion;
            if (aggregate.CountOfChangesFromLastSnapshot >= GlobalSettings.Current.CountOfChangesBeforeSnapshot)
            {
                aggregate.Apply(new BaseSnapshotEvent<TRoot> { SerializedAggregate = aggregate, Version = version });
            }

            this.storage.SaveEvents(aggregate.Id, aggregate.GetUncommittedChanges(), version);
        }
    }
}
