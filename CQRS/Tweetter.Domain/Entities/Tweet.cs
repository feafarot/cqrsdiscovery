﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tweet.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.Entities
{
    using System;
    using System.Reflection;

    using Tweetter.Cqrs.Core;
    using Tweetter.Cqrs.Core.Events;
    using Tweetter.Cqrs.Core.Utils;
    using Tweetter.Domain.EntityEvents;

    /// <summary>
    /// Tweet entity.
    /// </summary>
    [Serializable]
    public class Tweet : AggregateRoot
    {
        private Guid tweetId;

        private string body;

        private DateTime lastDateChanged;

        private Guid postedByUserId;

        private bool canCycle = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tweet"/> class.
        /// </summary>
        public Tweet()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tweet"/> class.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <param name="postedByUserId">The posted by user id.</param>
        public Tweet(string body, Guid postedByUserId)
        {
            this.ApplyChange(new TweetCreatedEvent(Guid.NewGuid(), body, postedByUserId, DateTime.Now));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tweet"/> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="body">The body.</param>
        /// <param name="lastDateChanged">The last date changed.</param>
        /// <param name="postedByUserId">The posted by user id.</param>
        public Tweet(Guid id, string body, DateTime lastDateChanged, Guid postedByUserId)
        {
            this.tweetId = id;
            this.body = body;
            this.lastDateChanged = lastDateChanged;
            this.postedByUserId = postedByUserId;
        }

        /// <summary>
        /// Gets the unique item id.
        /// </summary>
        public override Guid Id
        {
            get
            {
                return this.tweetId;
            }
        }

        /// <summary>
        /// Recreates this instance.
        /// </summary>
        public override void Recreate()
        {
            this.ApplyChange(new TweetCreatedEvent(this.tweetId, this.body, this.postedByUserId, this.lastDateChanged));
        }

        /// <summary>
        /// Changes the body.
        /// </summary>
        /// <param name="newBody">The new body.</param>
        public void ChangeBody(string newBody)
        {
            ExceptionsHelper.CheckStringForEmpy(newBody, "'newBody' mustn't be empty.");
            this.ApplyChange(new TweetBodyChangedEvent(this.tweetId, newBody, DateTime.Now));
        }

        /// <summary>
        /// Applies the specified event changes.
        /// </summary>
        /// <param name="event">The event that contains changes.</param>
        public void Apply(TweetBodyChangedEvent @event)
        {
            if (this.tweetId == @event.TweetId)
            {
                this.body = @event.NewBody;
                this.lastDateChanged = @event.LastDateChanged;
            }
        }

        /// <summary>
        /// Applies the specified event changes.
        /// </summary>
        /// <param name="event">The event that contains changes.</param>
        public void Apply(TweetCreatedEvent @event)
        {
            this.tweetId = @event.TweetId;
            this.body = @event.Body;
            this.postedByUserId = @event.PostedByUserId;
            this.lastDateChanged = @event.LastDateChanged;
        }
    }
}