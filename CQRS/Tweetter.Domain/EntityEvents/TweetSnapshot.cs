﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TweetSnapshot.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;
    using Tweetter.Domain.Entities;

    /// <summary>
    /// Snapshot of tweet.
    /// </summary>
    public class TweetSnapshot : BaseSnapshotEvent<Tweet>
    {
    }
}
