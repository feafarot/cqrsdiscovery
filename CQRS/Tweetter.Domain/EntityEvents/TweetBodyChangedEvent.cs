﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TweetBodyChangedEvent.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Tweet newBody changed event.
    /// </summary>
    public class TweetBodyChangedEvent : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TweetBodyChangedEvent"/> class.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        /// <param name="newBody">The newBody.</param>
        /// <param name="lastDateChanged">The last date changed.</param>
        public TweetBodyChangedEvent(Guid tweetId, string newBody, DateTime lastDateChanged)
        {
            this.TweetId = tweetId;
            this.NewBody = newBody;
            this.LastDateChanged = lastDateChanged;
        }

        /// <summary>
        /// Gets or sets the tweet id.
        /// </summary>
        public Guid TweetId { get; set; }

        /// <summary>
        /// Gets or sets the newBody.
        /// </summary>
        public string NewBody { get; set; }

        /// <summary>
        /// Gets or sets the last date changed.
        /// </summary>`
        public DateTime LastDateChanged { get; set; }
    }
}
