﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RestoreTweetEvent.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Restore tweet event.
    /// </summary>
    public class RestoreTweetEvent : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreTweetEvent"/> class.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        /// <param name="version">The version.</param>
        public RestoreTweetEvent(Guid tweetId, int version)
        {
            this.TweetId = tweetId;
            this.Version = version;
        }

        /// <summary>
        /// Gets or sets the tweet id.
        /// </summary>
        public Guid TweetId { get; set; }

        /// <summary>
        /// Gets or sets event version.
        /// </summary>
        public int Version { get; set; }
    }
}
