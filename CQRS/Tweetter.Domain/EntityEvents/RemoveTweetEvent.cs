﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RemoveTweetEvent.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Remove tweet event.
    /// </summary>
    public class RemoveTweetEvent : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoveTweetEvent"/> class.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        public RemoveTweetEvent(Guid tweetId)
        {
        }

        /// <summary>
        /// Gets or sets the tweet id.
        /// </summary>
        public Guid TweetId { get; set; }
    }
}
