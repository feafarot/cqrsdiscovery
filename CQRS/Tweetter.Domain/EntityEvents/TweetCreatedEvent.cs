﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TweetCreatedEvent.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Events;

    /// <summary>
    /// Tweet created event.
    /// </summary>
    public class TweetCreatedEvent : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TweetCreatedEvent"/> class.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        /// <param name="body">The body.</param>
        /// <param name="postedByUserId">The posted by user id.</param>
        /// <param name="lastDateChanged">The last date changed.</param>
        public TweetCreatedEvent(Guid tweetId, string body, Guid postedByUserId, DateTime lastDateChanged)
        {
            this.TweetId = tweetId;
            this.Body = body;
            this.PostedByUserId = postedByUserId;
            this.LastDateChanged = lastDateChanged;
        }

        /// <summary>
        /// Gets or sets the tweet id.
        /// </summary>
        public Guid TweetId { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the last date changed.
        /// </summary>
        public DateTime LastDateChanged { get; set; }

        /// <summary>
        /// Gets or sets the posted by user id.
        /// </summary>
        public Guid PostedByUserId { get; set; }
    }
}
