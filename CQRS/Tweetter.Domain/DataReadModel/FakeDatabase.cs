﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FakeDatabase.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.DataReadModel
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Fake database.
    /// </summary>
    public class FakeTweetDatabase : IDatabase<TweetDto>
    {
        private static readonly Dictionary<Guid, TweetDto> Tweets;

        /// <summary>
        /// Initializes static members of the <see cref="FakeTweetDatabase"/> class.
        /// </summary>
        static FakeTweetDatabase()
        {
            Tweets = new Dictionary<Guid, TweetDto>();
        }

        /// <summary>
        /// Adds the Tweet.
        /// </summary>
        /// <param name="tweetDto">The tweet dto.</param>
        public void AddEntity(TweetDto tweetDto)
        {
            if (Tweets.ContainsKey(tweetDto.Id))
            {
                throw new InvalidOperationException("Tweet can't be added becouse tweet with the same Id are already exist.");
            }

            Tweets.Add(tweetDto.Id, tweetDto);
        }

        /// <summary>
        /// Gets the entity by id.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        /// <returns>Returns the tweet dto.</returns>
        public TweetDto GetEntityById(Guid tweetId)
        {
            if (!Tweets.ContainsKey(tweetId))
            {
                throw new InvalidOperationException("Can't find tweet.");
            }

            return Tweets[tweetId];
        }

        /// <summary>
        /// Gets all tweets.
        /// </summary>
        /// <returns>Returns all tweets.</returns>
        public IEnumerable<TweetDto> GetAllEntities()
        {
            return Tweets.Values;
        }

        /// <summary>
        /// Removes the entity by id.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        public void RemoveEntityById(Guid tweetId)
        {
            if (!Tweets.ContainsKey(tweetId))
            {
                throw new InvalidOperationException("Can't find tweet.");
            }

            Tweets.Remove(tweetId);
        }
    }
}
