﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReadModelFacade.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.DataReadModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Read model facade.
    /// </summary>
    public interface IReadModelFacade
    {
        /// <summary>
        /// Gets the tweet detail by it id.
        /// </summary>
        /// <param name="id">The tweet id.</param>
        /// <returns>Returns detail tweet dto.</returns>
        TweetDto GetTweetDtolById(Guid id);

        /// <summary>
        /// Gets all tweets.
        /// </summary>
        /// <returns>Tweet dtos collention.</returns>
        IEnumerable<TweetDto> GetAllTweets();
    }
}
