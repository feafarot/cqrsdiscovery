﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataBase.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.DataReadModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Sample database interfase, or this is a repository.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IDatabase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Adds the entity.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        void AddEntity(TEntity entity);

        /// <summary>
        /// Gets the entity by its id.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>Returns entity by id.</returns>
        TEntity GetEntityById(Guid entityId);

        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns>All stored entities.</returns>
        IEnumerable<TEntity> GetAllEntities();

        /// <summary>
        /// Removes the entity by its id.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        void RemoveEntityById(Guid entityId);
    }
}
