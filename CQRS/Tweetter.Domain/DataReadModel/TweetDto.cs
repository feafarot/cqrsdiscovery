﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TweetDto.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.DataReadModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Tweet data transfer object.
    /// </summary>
    public class TweetDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TweetDto"/> class.
        /// </summary>
        /// <remarks>
        /// Don't use this constructor. It's needed for correct work of MVC.
        /// </remarks>
        public TweetDto()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TweetDto"/> class.
        /// </summary>
        /// <param name="id">The tweet id.</param>
        /// <param name="body">The body.</param>
        /// <param name="lastDateChanged">The last date changed.</param>
        /// <param name="postedByUser">The posted by user.</param>
        /// <param name="version">The version.</param>
        public TweetDto(
            Guid id, 
            string body, 
            DateTime lastDateChanged, 
            Guid postedByUser,
            int version)
        {
            this.Id = id;
            this.Body = body;
            this.LastDateChanged = lastDateChanged;
            this.PostedByUserId = postedByUser;
            this.Version = version;
        }

        /// <summary>
        /// Gets or sets the id of tweet.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the tweet body.
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the last change date of tweet.
        /// </summary>
        public DateTime LastDateChanged { get; set; }

        /// <summary>
        /// Gets or sets the posted by user id.
        /// </summary>
        public Guid PostedByUserId { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public int Version { get; set; }
    }
}
