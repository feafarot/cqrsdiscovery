﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReadModelFacade.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.DataReadModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Fakse sample read model.
    /// </summary>
    public class ReadModelFacade : IReadModelFacade
    {
        private IDatabase<TweetDto> database;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadModelFacade"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public ReadModelFacade(IDatabase<TweetDto> database)
        {
            this.database = database;
        }

        /// <summary>
        /// Gets the tweet detail by it id.
        /// </summary>
        /// <param name="id">The tweet id.</param>
        /// <returns>
        /// Returns tweet dto.
        /// </returns>
        public TweetDto GetTweetDtolById(Guid id)
        {
            return this.database.GetEntityById(id);
        }

        /// <summary>
        /// Gets all tweets.
        /// </summary>
        /// <returns>
        /// Tweet dtos collention.
        /// </returns>
        public IEnumerable<TweetDto> GetAllTweets()
        {
            return this.database.GetAllEntities();
        }
    }
}
