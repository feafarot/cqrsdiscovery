﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TweetDetailView.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.DataReadModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core;
    using Tweetter.Cqrs.Core.ReadDataModel;
    using Tweetter.Domain.EntityEvents;

    /// <summary>
    /// Tweet detail view.
    /// </summary>
    public class TweetDetailView : IHandles<TweetCreatedEvent>, IHandles<TweetBodyChangedEvent>
    {
        private readonly IDatabase<TweetDto> database;

        /// <summary>
        /// Initializes a new instance of the <see cref="TweetDetailView"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public TweetDetailView(IDatabase<TweetDto> database)
        {
            this.database = database;
        }

        /// <summary>
        /// Handles the tweet created event.
        /// </summary>
        /// <param name="message">The event message.</param>
        public void Handle(TweetCreatedEvent message)
        {
            this.database.AddEntity(new TweetDto(
                                                message.TweetId, 
                                                message.Body, 
                                                message.LastDateChanged, 
                                                message.PostedByUserId, 
                                                message.Version));
        }

        /// <summary>
        /// Handles the tweet body changed event.
        /// </summary>
        /// <param name="message">The event message.</param>
        public void Handle(TweetBodyChangedEvent message)
        {
            var tweetDto = this.database.GetEntityById(message.TweetId);
            tweetDto.Body = message.NewBody;
            tweetDto.Version = message.Version;
        }
    }
}
