﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateTweetCommand.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityCommands
{
    using System;

    using Tweetter.Cqrs.Core.Commands;

    /// <summary>
    /// Create tweet command.
    /// </summary>
    public class CreateTweetCommand : Command
    {
        /// <summary>
        /// Tweet body.
        /// </summary>
        public readonly string Body;

        /// <summary>
        /// UserId by who was posted tweet.
        /// </summary>
        public readonly Guid PostedByUserId;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateTweetCommand"/> class.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <param name="postedByUserId">The posted by user id.</param>
        public CreateTweetCommand(string body, Guid postedByUserId)
        {
            this.Body = body;
            this.PostedByUserId = postedByUserId;
        }
    }
}
