﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TweetCommandHandlers.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityCommands.Handlers
{
    using Tweetter.Cqrs.Core.Data;
    using Tweetter.Domain.Entities;
    using Tweetter.Domain.EntityEvents;

    /// <summary>
    /// Handlers for tweet commands.
    /// </summary>
    public class TweetCommandHandlers
    {
        private readonly IRepository<Tweet> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TweetCommandHandlers"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public TweetCommandHandlers(IRepository<Tweet> repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Handles the specified tweet created event.
        /// </summary>
        /// <param name="createTweetCommand">The tweet created event.</param>
        public void Handle(CreateTweetCommand createTweetCommand)
        {
            var tweet = new Tweet(createTweetCommand.Body, createTweetCommand.PostedByUserId);
            this.repository.Save(tweet, -1);
        }

        /// <summary>
        /// Handles the specified tweet body changed event.
        /// </summary>
        /// <param name="changeTweetBodyCommand">The change tweet body command.</param>
        public void Handle(ChangeTweetBodyCommand changeTweetBodyCommand)
        {
            var tweet = this.repository.GetById(changeTweetBodyCommand.TweetId);
            tweet.ChangeBody(changeTweetBodyCommand.NewBody);
            this.repository.Save(tweet, changeTweetBodyCommand.OriginalVersion);
        }
    }
}
