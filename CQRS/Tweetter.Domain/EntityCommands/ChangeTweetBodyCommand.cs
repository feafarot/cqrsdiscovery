﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangeTweetBodyCommand.cs" company="Sly Team">
//   No any license yet...
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tweetter.Domain.EntityCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Tweetter.Cqrs.Core.Commands;

    /// <summary>
    /// Change body of tweet command 
    /// </summary>
    public class ChangeTweetBodyCommand : Command
    {
        /// <summary>
        /// Id of tweet.
        /// </summary>
        public readonly Guid TweetId;

        /// <summary>
        /// New tweet body.
        /// </summary>
        public readonly string NewBody;

        /////// <summary>
        /////// Last change date of tweet.
        /////// </summary>
        ////public readonly DateTime LastDateChanged;

        /// <summary>
        /// Tweet original version.
        /// </summary>
        public readonly int OriginalVersion;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeTweetBodyCommand"/> class.
        /// </summary>
        /// <param name="tweetId">The tweet id.</param>
        /// <param name="newBody">The new body.</param>
        /// <param name="originalVersion">The original version.</param>
        public ChangeTweetBodyCommand(Guid tweetId, string newBody, int originalVersion)
        {
            this.TweetId = tweetId;
            this.OriginalVersion = originalVersion;
            this.NewBody = newBody;
        }
    }
}
